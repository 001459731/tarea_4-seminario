﻿// Realizar una operación de partición en una lista de objetos basada en una condición. (3pts)
using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        // Definir una lista de objetos de ejemplo
        List<Persona> personas = new List<Persona>()
        {
            new Persona() { Nombre = "Juan", Edad = 25 },
            new Persona() { Nombre = "María", Edad = 30 },
            new Persona() { Nombre = "Pedro", Edad = 35 },
            new Persona() { Nombre = "Ana", Edad = 40 }
        };

        // Definir una condición para la partición
        Func<Persona, bool> condicion = persona => persona.Edad >= 30;

        // Realizar la partición de la lista basada en la condición
        Tuple<List<Persona>, List<Persona>> particion = Particionar(personas, condicion);

        // Obtener las sublistas resultantes
        List<Persona> mayoresDe30 = particion.Item1;
        List<Persona> menoresDe30 = particion.Item2;

        // Imprimir los resultados
        Console.WriteLine("Personas mayores o iguales a 30 años:");
        foreach (var persona in mayoresDe30)
        {
            Console.WriteLine("Nombre: " + persona.Nombre + ", Edad: " + persona.Edad);
        }

        Console.WriteLine("\nPersonas menores de 30 años:");
        foreach (var persona in menoresDe30)
        {
            Console.WriteLine("Nombre: " + persona.Nombre + ", Edad: " + persona.Edad);
        }
    }

    // Método para realizar la partición de una lista de objetos basada en una condición
    static Tuple<List<Persona>, List<Persona>> Particionar(List<Persona> lista, Func<Persona, bool> condicion)
    {
        List<Persona> cumplenCondicion = new List<Persona>(); // Lista para los elementos que cumplen la condición
        List<Persona> noCumplenCondicion = new List<Persona>(); // Lista para los elementos que no cumplen la condición

        foreach (var elemento in lista)
        {
            if (condicion(elemento))
            {
                cumplenCondicion.Add(elemento); // Agregar a la lista de cumplenCondicion si la condición es verdadera
            }
            else
            {
                noCumplenCondicion.Add(elemento); // Agregar a la lista de noCumplenCondicion si la condición es falsa
            }
        }

        return Tuple.Create(cumplenCondicion, noCumplenCondicion); // Devolver las sublistas como una tupla
    }
}

// Clase Persona para representar objetos de la lista
class Persona
{
    public string Nombre { get; set; }
    public int Edad { get; set; }
}


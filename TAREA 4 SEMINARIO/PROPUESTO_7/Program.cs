﻿//Realizar una operación de unión completa entre dos listas de objetos. (3pts)
using System; // Directiva para acceder al espacio de nombres System
using System.Collections.Generic; // Directiva para acceder al espacio de nombres System.Collections.Generic

class Program // Declaración de la clase Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        // Definir dos listas de objetos con una propiedad común
        List<Persona> lista1 = new List<Persona>() // Declara e inicializa una lista de objetos de tipo Persona
        {
            new Persona() { Nombre = "Juan", Edad = 25 }, // Agrega un objeto Persona a la lista con nombre "Juan" y edad 25
            new Persona() { Nombre = "María", Edad = 30 } // Agrega otro objeto Persona a la lista con nombre "María" y edad 30
        };

        List<Persona> lista2 = new List<Persona>() // Declara e inicializa otra lista de objetos de tipo Persona
        {
            new Persona() { Nombre = "Pedro", Edad = 35 }, // Agrega un objeto Persona a la lista con nombre "Pedro" y edad 35
            new Persona() { Nombre = "Ana", Edad = 40 } // Agrega otro objeto Persona a la lista con nombre "Ana" y edad 40
        };

        // Realizar una operación de unión completa entre las dos listas
        List<Persona> unionCompleta = UnionCompleta(lista1, lista2);

        // Imprimir el resultado
        Console.WriteLine("Unión completa de las dos listas:");
        foreach (Persona persona in unionCompleta)
        {
            Console.WriteLine("Nombre: " + persona.Nombre + ", Edad: " + persona.Edad);
        }
    }

    // Método para realizar una operación de unión completa entre dos listas de objetos
    static List<Persona> UnionCompleta(List<Persona> lista1, List<Persona> lista2)
    {
        List<Persona> union = new List<Persona>(); // Crear una nueva lista para almacenar la unión completa

        union.AddRange(lista1); // Agregar todos los elementos de la primera lista a la lista de unión

        foreach (Persona persona in lista2) // Iterar sobre cada elemento en la segunda lista
        {
            if (!union.Contains(persona)) // Verificar si el elemento no está en la lista de unión
            {
                union.Add(persona); // Si no está, agregarlo a la lista de unión
            }
        }

        return union; // Devolver la lista de unión completa
    }
}

// Definir una clase "Persona" con propiedades "Nombre" y "Edad"
class Persona // Declaración de la clase Persona
{
    public string Nombre { get; set; } // Propiedad Nombre de la Persona
    public int Edad { get; set; } // Propiedad Edad de la Persona
}

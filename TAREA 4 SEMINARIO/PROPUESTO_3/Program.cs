﻿// Filtrar una lista de objetos por un rango de valores de una propiedad numérica. (2 pts)

using System; // Directiva para acceder al espacio de nombres System
using System.Collections.Generic; // Directiva para acceder al espacio de nombres System.Collections.Generic
using System.Linq; // Directiva para acceder al espacio de nombres System.Linq

class Program // Declaración de la clase Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        // Definir una lista de objetos con una propiedad numérica
        List<Persona> personas = new List<Persona>() // Declara e inicializa una lista de objetos de tipo Persona
        {
            new Persona() { Nombre = "Juan", Edad = 25 }, // Agrega un objeto Persona a la lista con nombre "Juan" y edad 25
            new Persona() { Nombre = "María", Edad = 30 }, // Agrega otro objeto Persona a la lista con nombre "María" y edad 30
            new Persona() { Nombre = "Pedro", Edad = 35 }, // Agrega otro objeto Persona a la lista con nombre "Pedro" y edad 35
            new Persona() { Nombre = "Ana", Edad = 40 } // Agrega otro objeto Persona a la lista con nombre "Ana" y edad 40
        };

        // Definir un rango de valores para filtrar
        int edadMinima = 28; // Declara e inicializa la edad mínima del rango
        int edadMaxima = 35; // Declara e inicializa la edad máxima del rango

        // Filtrar la lista de personas por el rango de valores de la propiedad "Edad"
        List<Persona> personasFiltradas = personas.Where(p => p.Edad >= edadMinima && p.Edad <= edadMaxima).ToList();
        // Utiliza el método Where de LINQ para filtrar las personas cuya edad esté entre la edad mínima y máxima, y convierte el resultado a una lista

        // Imprimir el resultado
        Console.WriteLine("Personas entre " + edadMinima + " y " + edadMaxima + " años:");
        // Imprime un mensaje indicando el rango de edad que se ha filtrado

        foreach (var persona in personasFiltradas) // Inicia un bucle foreach para iterar sobre cada persona en la lista filtrada
        {
            Console.WriteLine("Nombre: " + persona.Nombre + ", Edad: " + persona.Edad); // Imprime el nombre y la edad de cada persona en la lista filtrada
        }
    }
}

// Definir una clase "Persona" con propiedades "Nombre" y "Edad"
class Persona // Declaración de la clase Persona
{
    public string Nombre { get; set; } // Propiedad Nombre de la Persona
    public int Edad { get; set; } // Propiedad Edad de la Persona
}


﻿// See https://aka.ms/new-console-template for more information
using System; // Directiva para acceder al espacio de nombres System
using System.Collections.Generic; // Directiva para acceder al espacio de nombres System.Collections.Generic
using System.Linq; // Directiva para acceder al espacio de nombres System.Linq

class Program // Declaración de la clase Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        List<int> numeros = new List<int>() { 1, 2, 3, 4, 5 }; // Declaración e inicialización de una lista con valores enteros

        int sumaCuadrados = numeros.Sum(numero => numero * numero); // Calcula la suma de los cuadrados de los números en la lista

        Console.WriteLine("La suma de los cuadrados de los números en la lista es: " + sumaCuadrados); // Imprime la suma de los cuadrados en la consola
    }
}

﻿// 11.	Escriba un programa en C# para mostrar los registros número n superiores.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los miembros de la lista
        List<int> numeros = new List<int> { 5, 7, 13, 24, 6, 9, 8, 7 };

        // Solicitar al usuario el número de registros a mostrar
        Console.Write("¿Cuántos registros desea mostrar? : ");
        int n = int.Parse(Console.ReadLine());

        // Utilizar LINQ para ordenar la lista en orden descendente y tomar los primeros 'n' registros
        var registrosTop = numeros.OrderByDescending(num => num).Take(n);

        // Mostrar los registros número 'n' superiores
        Console.WriteLine($"Los {n} primeros registros de la lista son:");
        foreach (var registro in registrosTop)
        {
            Console.WriteLine(registro);
        }
    }
}


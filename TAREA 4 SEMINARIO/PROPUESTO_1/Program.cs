﻿// Concatenar dos listas eliminando duplicados. (2 pts)
using System; // Directiva using para acceder a tipos y miembros en el espacio de nombres System
using System.Collections.Generic; // Directiva using para acceder a tipos y miembros en el espacio de nombres System.Collections.Generic
using System.Linq; // Directiva using para acceder a tipos y miembros en el espacio de nombres System.Linq

class Program // Declaración de una nueva clase llamada Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        List<int> lista1 = new List<int>() { 1, 2, 3, 4, 5 }; // Declaración de una nueva lista llamada lista1, inicializada con algunos valores
        List<int> lista2 = new List<int>() { 3, 4, 5, 6, 7 }; // Declaración de una nueva lista llamada lista2, inicializada con algunos valores

        // Concatenar las dos listas y eliminar duplicados
        List<int> listaConcatenada = lista1.Concat(lista2).Distinct().ToList();

        // Mostrar el resultado
        Console.WriteLine("Lista Concatenada sin duplicados:"); // Imprime un mensaje en la consola
        foreach (var item in listaConcatenada) // Iniciamos un bucle foreach para recorrer cada elemento de la listaConcatenada
        {
            Console.WriteLine(item); // Imprimimps cada elemento de la listaConcatenada en la consola
        }
    }
}

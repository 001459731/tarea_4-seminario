﻿//17.	Escriba un programa en C# para eliminar elementos de la lista usando la función eliminar pasando el objeto.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> lista = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in lista)
        {
            Console.WriteLine($"Char: {elemento}");
        }

        // Eliminar el elemento 'o' de la lista utilizando LINQ
        lista = lista.Where(elemento => elemento != 'o').ToList();

        // Mostrar la lista después de eliminar el elemento 'o'
        Console.WriteLine("Aquí está la lista después de eliminar el elemento 'o' de la lista:");
        foreach (var elemento in lista)
        {
            Console.WriteLine($"Char: {elemento}");
        }
    }
}


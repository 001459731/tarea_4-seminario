﻿// 25.	Escriba un programa en C# para generar una unión interna entre dos conjuntos de datos.
using System;
using System.Collections.Generic;
using System.Linq; // LINQ se utiliza aquí

class Program
{
    static void Main(string[] args)
    {
        // Conjunto 1: ID del artículo, Nombre del artículo, Cantidad de compra
        Console.WriteLine("Ingrese los datos para el primer conjunto:");
        List<(int, string, int)> conjunto1 = ObtenerConjunto();

        // Conjunto 2: ID del artículo, Nombre del artículo, Cantidad de compra
        Console.WriteLine("\nIngrese los datos para el segundo conjunto:");
        List<(int, string, int)> conjunto2 = ObtenerConjunto();

        // Realizar la unión interna entre los conjuntos utilizando LINQ
        var union = from item1 in conjunto1
                    join item2 in conjunto2
                    on item1.Item1 equals item2.Item1
                    select new
                    {
                        IDArticulo = item1.Item1,
                        NombreArticulo = item1.Item2,
                        CantidadCompra = item1.Item3
                    };

        // Mostrar el resultado
        Console.WriteLine("\nResultado de la unión interna:");
        Console.WriteLine("ID del artículo\tNombre del artículo\tCantidad de compra");
        foreach (var item in union)
        {
            Console.WriteLine($"{item.IDArticulo}\t\t{item.NombreArticulo}\t\t{item.CantidadCompra}");
        }
    }

    // Método para obtener los datos del conjunto
    static List<(int, string, int)> ObtenerConjunto()
    {
        List<(int, string, int)> conjunto = new List<(int, string, int)>();

        Console.Write("Ingrese el número de elementos en el conjunto: ");
        int n = int.Parse(Console.ReadLine());

        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Elemento {i + 1}:");
            Console.Write("ID del artículo: ");
            int id = int.Parse(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = int.Parse(Console.ReadLine());

            conjunto.Add((id, nombre, cantidad));
        }

        return conjunto;
    }
}


﻿// 27.	Escriba un programa en C# para generar una unión externa derecha entre dos conjuntos de datos.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Entrada de datos para el conjunto de artículos
        Console.WriteLine("Ingrese los datos para el conjunto de artículos:");
        Console.Write("Ingrese el número de artículos: ");
        int numArticulos = Convert.ToInt32(Console.ReadLine());

        List<(int, string, int)> articulos = new List<(int, string, int)>();

        for (int i = 1; i <= numArticulos; i++)
        {
            Console.WriteLine($"Artículo {i}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            articulos.Add((id, nombre, cantidad));
        }

        // Se realiza la operación de unión externa derecha entre la lista de articulos ingresados por el usuario y la lista de datos previamente almacenados, utilizando LINQ.
        var resultado = from articulo in articulos
                        join dato in datos on articulo.Item1 equals dato.Item1 into joined
                        from j in joined.DefaultIfEmpty((0, "", 0))
                        select (IDArticulo: j.Item1, NombreArticulo: j.Item2, CantidadCompra: j.Item3);

        // Salida de resultados
        Console.WriteLine("\nAquí está la lista después de unirse:\n");
        Console.WriteLine("ID del artículo  | Nombre del artículo | Cantidad de compra");
        Console.WriteLine("-----------------------------------------------");

        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.IDArticulo,-17} | {item.NombreArticulo,-21} | {item.CantidadCompra,-17}");
        }
    }
}


﻿// 5.	Escriba un programa en C# para mostrar los caracteres y la frecuencia de los caracteres de una cadena.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario que ingrese una cadena
        Console.Write("Ingrese la cadena: ");
        string cadena = Console.ReadLine();

        // Consulta LINQ para agrupar los caracteres y contar su frecuencia
        var frecuenciaCaracteres = cadena.GroupBy(c => c)
                                         .Select(group => new { Caracter = group.Key, Frecuencia = group.Count() });

        // Mostrar el resultado
        Console.WriteLine("La frecuencia de los caracteres es:");
        foreach (var item in frecuenciaCaracteres)
        {
            Console.WriteLine($"Carácter {item.Caracter}: {item.Frecuencia} veces");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}

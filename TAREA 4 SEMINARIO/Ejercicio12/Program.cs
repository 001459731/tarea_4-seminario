﻿// 12.	Escriba un programa en C# para encontrar las palabras en mayúsculas en una cadena.
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario que ingrese una cadena
        Console.Write("Ingrese la cadena: ");
        string cadena = Console.ReadLine();

        // Dividir la cadena en palabras
        string[] palabras = cadena.Split(' ');

        // Utilizar LINQ para encontrar las palabras en mayúsculas
        var palabrasMayusculas = palabras.Where(palabra => palabra == palabra.ToUpper());

        // Mostrar las palabras en mayúsculas
        Console.WriteLine("Las palabras en MAYÚSCULAS son:");
        foreach (var palabra in palabrasMayusculas)
        {
            Console.WriteLine(palabra);
        }
    }
}


﻿// Eliminar elementos duplicados de una lista mientras se conserva el orden original. (3pts)
using System; // Directiva para acceder al espacio de nombres System
using System.Collections.Generic; // Directiva para acceder al espacio de nombres System.Collections.Generic

class Program // Declaración de la clase Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        List<int> lista = new List<int>() { 1, 2, 3, 2, 4, 1, 5 }; // Definir una lista con elementos duplicados

        // Eliminar duplicados conservando el orden original
        List<int> listaSinDuplicados = EliminarDuplicados(lista);

        // Imprimir el resultado
        Console.WriteLine("Lista sin duplicados:");
        foreach (int elemento in listaSinDuplicados)
        {
            Console.WriteLine(elemento);
        }
    }

    // Método para eliminar elementos duplicados conservando el orden original
    static List<int> EliminarDuplicados(List<int> lista)
    {
        List<int> listaSinDuplicados = new List<int>(); // Crear una nueva lista para almacenar elementos únicos

        HashSet<int> hashSet = new HashSet<int>(); // Crear un HashSet para mantener un registro de los elementos únicos

        // Iterar sobre la lista original para eliminar duplicados y conservar el orden original
        foreach (int elemento in lista)
        {
            if (!hashSet.Contains(elemento)) // Verificar si el elemento ya está en el HashSet
            {
                hashSet.Add(elemento); // Si no está, agregarlo al HashSet
                listaSinDuplicados.Add(elemento); // Agregar el elemento a la lista sin duplicados
            }
        }

        return listaSinDuplicados; // Devolver la lista sin duplicados
    }
}


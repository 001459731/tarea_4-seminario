﻿// Encontrar la moda (valor que aparece con mayor frecuencia) en una lista de números. (3pts)
using System; // Directiva para acceder al espacio de nombres System
using System.Collections.Generic; // Directiva para acceder al espacio de nombres System.Collections.Generic
using System.Linq; // Directiva para acceder al espacio de nombres System.Linq

class Program // Declaración de la clase Program
{
    static void Main() // Declaración del método Main, punto de entrada del programa
    {
        List<int> numeros = new List<int>() { 1, 2, 3, 4, 4, 4, 5, 5, 6 }; // Definir una lista de números

        // Calcular la moda de la lista de números
        int moda = CalcularModa(numeros);

        // Imprimir el resultado
        Console.WriteLine("La moda de la lista de números es: " + moda);
    }

    // Método para calcular la moda de una lista de números
    static int CalcularModa(List<int> numeros)
    {
        // Crear un diccionario para contar la frecuencia de cada número
        Dictionary<int, int> frecuencia = new Dictionary<int, int>();

        // Iterar sobre la lista de números para contar las ocurrencias de cada número
        foreach (int numero in numeros)
        {
            if (frecuencia.ContainsKey(numero))
            {
                frecuencia[numero]++;
            }
            else
            {
                frecuencia[numero] = 1;
            }
        }

        // Encontrar el número con la frecuencia máxima (la moda)
        int moda = frecuencia.OrderByDescending(pair => pair.Value).First().Key;

        return moda; // Devolver la moda
    }
}


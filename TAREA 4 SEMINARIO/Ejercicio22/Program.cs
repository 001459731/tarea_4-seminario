﻿// 22.	Escriba un programa en C# para encontrar las cadenas para una longitud mínima específica.
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario ingresar el número de cadenas
        Console.WriteLine("Ingrese el número de cadenas que se almacenarán en la matriz:");
        int numCadenas = int.Parse(Console.ReadLine());

        // Crear una matriz para almacenar las cadenas ingresadas
        string[] cadenas = new string[numCadenas];

        // Solicitar al usuario que ingrese las cadenas
        for (int i = 0; i < numCadenas; i++)
        {
            Console.Write($"Elemento [{i}]: ");
            cadenas[i] = Console.ReadLine();
        }

        // Solicitar al usuario que ingrese la longitud mínima deseada
        Console.WriteLine("Ingrese la longitud mínima del elemento que desea encontrar:");
        int longitudMinima = int.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar las cadenas con longitud mínima especificada
        var cadenasFiltradas = cadenas.Where(c => c.Length >= longitudMinima);

        // Mostrar los elementos con longitud mínima especificada
        Console.WriteLine($"Los elementos de un mínimo de {longitudMinima} caracteres son:");
        foreach (var cadena in cadenasFiltradas)
        {
            Console.WriteLine($"Elemento: {cadena}");
        }
    }
}


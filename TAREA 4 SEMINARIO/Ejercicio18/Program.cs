﻿//18.	Escriba un programa en C# para eliminar elementos de la lista creando un objeto internamente mediante el filtrado.
using System;
using System.Collections.Generic;
using System.Linq; // Directiva using para LINQ

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de números
        List<int> numeros = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        // Filtrar números pares utilizando LINQ
        var numerosPares = numeros.Where(numero => numero % 2 == 0); // LINQ: Where

        // Proyectar los nombres a mayúsculas utilizando LINQ
        var nombres = new List<string> { "Juan", "María", "Carlos", "Ana" };
        var nombresMayusculas = nombres.Select(nombre => nombre.ToUpper()); // LINQ: Select

        // Ordenar los números en orden descendente utilizando LINQ
        var numerosOrdenados = numeros.OrderByDescending(numero => numero); // LINQ: OrderByDescending

        // Obtener el primer número de la lista utilizando LINQ
        var primerNumero = numeros.FirstOrDefault(); // LINQ: FirstOrDefault

        // Contar la cantidad de números en la lista utilizando LINQ
        var cantidadNumeros = numeros.Count(); // LINQ: Count

        // Agrupar personas por edad utilizando LINQ
        var personas = new List<Persona>
        {
            new Persona { Nombre = "Juan", Edad = 25 },
            new Persona { Nombre = "María", Edad = 30 },
            new Persona { Nombre = "Carlos", Edad = 25 },
            new Persona { Nombre = "Ana", Edad = 30 }
        };
        var gruposPorEdad = personas.GroupBy(persona => persona.Edad); // LINQ: GroupBy

        // Mostrar resultados
        Console.WriteLine("Números pares:");
        foreach (var numero in numerosPares)
        {
            Console.WriteLine(numero);
        }

        Console.WriteLine("\nNombres en mayúsculas:");
        foreach (var nombre in nombresMayusculas)
        {
            Console.WriteLine(nombre);
        }

        Console.WriteLine("\nNúmeros ordenados en orden descendente:");
        foreach (var numero in numerosOrdenados)
        {
            Console.WriteLine(numero);
        }

        Console.WriteLine($"\nPrimer número de la lista: {primerNumero}");
        Console.WriteLine($"Cantidad de números en la lista: {cantidadNumeros}");

        Console.WriteLine("\nGrupos de personas por edad:");
        foreach (var grupo in gruposPorEdad)
        {
            Console.WriteLine($"Edad: {grupo.Key}");
            foreach (var persona in grupo)
            {
                Console.WriteLine($"Nombre: {persona.Nombre}");
            }
            Console.WriteLine();
        }
    }
}

class Persona
{
    public string Nombre { get; set; }
    public int Edad { get; set; }
}


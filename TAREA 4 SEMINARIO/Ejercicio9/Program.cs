﻿// 9.	Escriba un programa en C# para crear una lista de números y mostrar los números mayores a 80 como salida.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los miembros de la lista
        List<int> numeros = new List<int> { 55, 200, 740, 76, 230, 482, 95 };

        // Utilizar LINQ para seleccionar los números mayores que 80
        var numerosMayores80 = numeros.Where(num => num > 80);

        // Mostrar los números mayores que 80
        Console.WriteLine("Los números mayores que 80 son:");
        foreach (var numero in numerosMayores80)
        {
            Console.WriteLine(numero);
        }
    }
}


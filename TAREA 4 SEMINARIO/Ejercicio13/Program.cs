﻿// 13.	Escriba un programa en C# para convertir una matriz de cadenas en una cadena
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario el número de cadenas y crear la matriz
        Console.Write("Ingrese el número de cadenas que se almacenarán en la matriz: ");
        int numCadenas = int.Parse(Console.ReadLine());
        string[] matriz = new string[numCadenas];

        // Solicitar al usuario que ingrese las cadenas para la matriz
        for (int i = 0; i < numCadenas; i++)
        {
            Console.Write($"Elemento [{i}]: ");
            matriz[i] = Console.ReadLine();
        }

        // Utilizar LINQ para concatenar las cadenas en una sola cadena
        string cadena = string.Join(", ", matriz);

        // Mostrar la cadena resultante
        Console.WriteLine("Aquí está la cadena creada con elementos de la matriz anterior: " + cadena);
    }
}


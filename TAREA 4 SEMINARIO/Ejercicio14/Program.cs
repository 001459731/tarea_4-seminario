﻿//14.	Escriba un programa en C# para encontrar el punto número máximo de calificación alcanzado por los estudiantes de la lista de estudiantes.
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de estudiantes
        List<Estudiante> estudiantes = new List<Estudiante>
        {
            new Estudiante { Id = 1, Nombre = "Juan", Puntaje = 600 },
            new Estudiante { Id = 2, Nombre = "María", Puntaje = 700 },
            new Estudiante { Id = 3, Nombre = "Pedro", Puntaje = 750 },
            new Estudiante { Id = 4, Nombre = "Ana", Puntaje = 700 },
            new Estudiante { Id = 5, Nombre = "Carlos", Puntaje = 800 },
            new Estudiante { Id = 6, Nombre = "Laura", Puntaje = 720 },
            new Estudiante { Id = 7, Nombre = "David", Puntaje = 750 },
            new Estudiante { Id = 8, Nombre = "Sofía", Puntaje = 720 },
            new Estudiante { Id = 9, Nombre = "Jorge", Puntaje = 670 },
            new Estudiante { Id = 10, Nombre = "Jenny", Puntaje = 750 }
        };

        // Solicitar al usuario el puntaje máximo deseado
        Console.Write("Qué puntaje máximo desea encontrar? : ");
        int puntajeMaximo = int.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar los estudiantes con el puntaje máximo deseado
        var estudiantesMaximoPuntaje = estudiantes.FindAll(estudiante => estudiante.Puntaje == puntajeMaximo);

        // Mostrar los estudiantes con el puntaje máximo deseado
        Console.WriteLine($"Los estudiantes con un puntaje máximo de {puntajeMaximo} son:");
        foreach (var estudiante in estudiantesMaximoPuntaje)
        {
            Console.WriteLine($"Id: {estudiante.Id}, Nombre: {estudiante.Nombre}, Puntaje alcanzado: {estudiante.Puntaje}");
        }
    }
}

class Estudiante
{
    public int Id { get; set; }
    public string Nombre { get; set; }
    public int Puntaje { get; set; }
}


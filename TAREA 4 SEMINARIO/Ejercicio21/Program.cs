﻿//21.	Escriba un programa en C# para eliminar un rango de elementos de una lista al pasar el índice de inicio y el número de elementos a eliminar.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> elementos = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();

        // Índice de inicio y número de elementos a eliminar
        int indiceInicio = 1;
        int numeroElementosEliminar = 3;

        // Utilizando LINQ para filtrar y mantener los elementos que no están en el rango especificado
        elementos = elementos.Where((e, index) => index < indiceInicio || index >= indiceInicio + numeroElementosEliminar).ToList();

        // Mostrar la lista después de la eliminación
        Console.WriteLine($"Aquí está la lista después de eliminar los {numeroElementosEliminar} elementos que comienzan del índice de elementos {indiceInicio} de la lista:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();
    }
}


﻿//3.	Escriba un programa en C# para encontrar el número de una matriz y el cuadrado de cada número.
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir una matriz de números
        int[] numeros = { 9, 8, 6, 5 };

        // Consulta LINQ para encontrar el número y el cuadrado de cada número en la matriz
        var numerosConCuadrado = from num in numeros
                                 select new { Numero = num, SqrNo = num * num };

        // Mostrar el resultado
        foreach (var item in numerosConCuadrado)
        {
            Console.WriteLine($"{{Número = {item.Numero}, SqrNo = {item.SqrNo}}}");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}

﻿//6.	Escriba un programa en C# para mostrar el nombre de los días de la semana.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Lista predefinida de nombres de días de la semana
        List<string> diasSemana = new List<string>
        {
            "domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"
        };

        // Mostrar los nombres de los días de la semana utilizando LINQ
        string diasSemanaString = string.Join(" ", diasSemana.Select(dia => dia));

        // Mostrar los nombres de los días de la semana en la consola
        Console.WriteLine(diasSemanaString);
    }
}


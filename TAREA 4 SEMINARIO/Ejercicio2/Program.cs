﻿// 2.	Escriba un programa en C# para encontrar los números de más de una lista de números usando dos condiciones en la consulta LINQ.
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir dos listas de números
        var lista1 = new int[] { 1, 3, 5, 7, 9, 11 };
        var lista2 = new int[] { 2, 3, 4, 6, 8, 10 };

        // Consulta LINQ para encontrar los números comunes en ambas listas dentro del rango de 1 a 11
        var numerosComunes = from num1 in lista1
                             from num2 in lista2
                             where num1 >= 1 && num1 <= 11 && num2 >= 1 && num2 <= 11
                             where num1 == num2
                             select num1;

        // Eliminar duplicados y ordenar los resultados
        var numerosUnicos = numerosComunes.Distinct().OrderBy(n => n);

        // Mostrar el resultado
        Console.WriteLine("Los números dentro del rango de 1 a 11 son:");
        foreach (var num in numerosUnicos)
        {
            Console.Write(num + " ");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}


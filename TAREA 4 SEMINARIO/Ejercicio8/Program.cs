﻿//8.	Escriba un programa en C# para encontrar la cadena que comienza y termina con un carácter específico.Console.WriteLine("Hello, World!");
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Lista de ciudades
        List<string> ciudades = new List<string> { "Roma", "LONDRES", "NAIROBI", "CALIFORNIA", "ZURICH", "Nueva Delhi", "AMSTERDAM", "ABU DHABI", "Paris" };

        // Solicitar al usuario que ingrese los caracteres de inicio y fin
        Console.Write("Ingrese el carácter de inicio para la cadena: ");
        char inicio = char.Parse(Console.ReadLine());
        Console.Write("Ingrese el carácter final para la cadena: ");
        char fin = char.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar la ciudad que cumple con los criterios
        string ciudadEncontrada = ciudades.FirstOrDefault(ciudad =>
            ciudad.StartsWith(inicio.ToString(), StringComparison.OrdinalIgnoreCase) &&
            ciudad.EndsWith(fin.ToString(), StringComparison.OrdinalIgnoreCase)
        );

        // Mostrar el resultado
        if (ciudadEncontrada != null)
        {
            Console.WriteLine($"La ciudad que comienza con {inicio} y termina con {fin} es: {ciudadEncontrada}");
        }
        else
        {
            Console.WriteLine("No se encontró ninguna ciudad que cumpla con los criterios.");
        }
    }
}

﻿// 15.	Escriba un programa en el programa C# para contar extensiones de archivo y agrúpelo usando LINQ.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de archivos
        List<string> archivos = new List<string> { "aaa.frx", "bbb.TXT", "xyz.dbf", "abc.pdf", "aaaa.PDF", "xyz.frt", "abc.xml", "ccc.txt", "zzz.txt" };

        // Utilizar LINQ para contar las extensiones de archivo y agruparlas
        var gruposExtensiones = archivos
            .Select(archivo => archivo.Substring(archivo.LastIndexOf('.') + 1).ToLower()) // Extraer y convertir las extensiones a minúsculas
            .GroupBy(extension => extension) // Agrupar por extensión
            .OrderBy(group => group.Key); // Ordenar por extensión

        // Mostrar el resultado
        Console.WriteLine("Aquí está el grupo de extensiones de los archivos:");
        foreach (var grupo in gruposExtensiones)
        {
            string archivoPlural = grupo.Count() == 1 ? "archivo" : "archivos";
            Console.WriteLine($"{grupo.Count()} {archivoPlural} con .{grupo.Key.ToUpper()} Extension");
        }
    }
}


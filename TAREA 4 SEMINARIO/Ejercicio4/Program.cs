﻿//4.	Escriba un programa en C# para mostrar el número y la frecuencia del número de la matriz.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir una matriz de números
        int[] numeros = { 5, 9, 5, 1, 5, 9 };

        // Consulta LINQ para agrupar los números y contar su frecuencia
        var frecuenciaNumeros = numeros.GroupBy(num => num)
                                       .Select(group => new { Numero = group.Key, Frecuencia = group.Count() });

        // Mostrar el resultado
        foreach (var item in frecuenciaNumeros)
        {
            Console.WriteLine($"el número {item.Numero} aparece {item.Frecuencia} veces");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}


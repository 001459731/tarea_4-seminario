﻿// 26.	Escriba un programa en C# para generar una unión izquierda entre dos conjuntos de datos.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        Console.WriteLine("Ingrese los datos para la unión izquierda:");
        Console.Write("Ingrese el número de elementos a agregar: ");
        int numElementos = Convert.ToInt32(Console.ReadLine());

        List<(int, string, int)> datos = new List<(int, string, int)>();

        for (int i = 1; i <= numElementos; i++)
        {
            Console.WriteLine($"Elemento {i}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            datos.Add((id, nombre, cantidad));
        }

        Console.WriteLine("\nIngrese los datos de los artículos:");
        Console.Write("Ingrese el número de artículos a agregar: ");
        int numArticulos = Convert.ToInt32(Console.ReadLine());

        // Arreglo para almacenar los artículos
        (int, string, int)[] articulos = new (int, string, int)[numArticulos];

        for (int i = 0; i < numArticulos; i++)
        {
            Console.WriteLine($"Artículo {i + 1}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            articulos[i] = (id, nombre, cantidad);
        }

        Console.WriteLine("\nAquí está la lista después de unirse:\n");
        Console.WriteLine("ID del artículo  | Nombre del artículo | Cantidad de compra");
        Console.WriteLine("-----------------------------------------------");

        //Esta consulta LINQ realiza una unión izquierda entre dos secuencias de datos: datos y articulos. 
        var resultado = from dato in datos
                        join articulo in articulos on dato.Item1 equals articulo.Item1 into joined
                        from j in joined.DefaultIfEmpty((0, "", 0))
                        select (IDArticulo: j.Item1, NombreArticulo: j.Item2, CantidadCompra: dato.Item3);

        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.IDArticulo,-17} | {item.NombreArticulo,-21} | {item.CantidadCompra,-17}");
        }
    }
}


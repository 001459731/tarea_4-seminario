﻿//7.	Escriba un programa en C# para mostrar números, multiplicación de números con la frecuencia y la frecuencia de un número de matriz de dar.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los números en la matriz
        int[] numeros = { 5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2 };

        // Consulta LINQ para agrupar los números y contar su frecuencia
        var resultado = numeros.GroupBy(num => num)
                               .Select(group => new
                               {
                                   Numero = group.Key,
                                   Frecuencia = group.Count(),
                                   Multiplicacion = group.Key * group.Count() // Multiplicar número por frecuencia
                               });

        // Mostrar el resultado
        Console.WriteLine("Número Frecuencia Número * Frecuencia");
        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.Numero,-7}{item.Frecuencia,-11}{item.Multiplicacion}");
        }
    }
}

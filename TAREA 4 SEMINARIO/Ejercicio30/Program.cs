﻿// 30.	Escriba un programa en C# para organizar los distintos elementos de la lista en orden ascendente.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de elementos
        List<string> elementos = new List<string> { "Butter", "Biscuit", "Honey", "Brade" };

        // Organizar los elementos en orden ascendente usando LINQ.
        var elementosOrdenados = elementos.OrderBy(elemento => elemento);

        // Imprimir los elementos ordenados
        Console.WriteLine("Resultado previsto:");
        foreach (var elemento in elementosOrdenados)
        {
            Console.WriteLine(elemento);
        }
    }
}

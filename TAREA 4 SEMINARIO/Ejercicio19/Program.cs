﻿// 19.	Escriba un programa en C# para eliminar elementos de la lista pasando filtros.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> elementos = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();

        // Eliminar el elemento 'q' de la lista utilizando LINQ
        elementos = elementos.Where(e => e != 'q').ToList(); // Utilizando LINQ para filtrar y crear una nueva lista

        // Mostrar la lista después de la eliminación
        Console.WriteLine("Aquí está la lista después de eliminar el elemento 'q' de la lista:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();
    }
}


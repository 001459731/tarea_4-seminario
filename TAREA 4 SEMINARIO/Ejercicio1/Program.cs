﻿// 1.	Escriba un programa en C# para mostrar cómo se ejecutan las tres partes de una operación de consulta.
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Crear una lista de números del 0 al 9
        var numeros = Enumerable.Range(0, 10);

        // Consulta LINQ para seleccionar números que producen un resto de 0 al dividir por 2
        var numerosPares = from num in numeros
                           where num % 2 == 0
                           select num;

        // Mostrar el resultado
        Console.WriteLine("Los números que producen el resto 0 después de dividido por 2 son:");
        foreach (var num in numerosPares)
        {
            Console.Write(num + " ");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}

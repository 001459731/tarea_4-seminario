﻿//16.	Escriba un programa en C# para calcular el tamaño del archivo usando LINQ.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: tamaños de archivo en MB
        List<double> tamanosArchivo = new List<double> { 2.5, 4.5, 3.0, 3.5, 2.0 };

        // Utilizar LINQ para calcular el tamaño promedio de archivo
        double tamanoPromedio = tamanosArchivo.Average();

        // Mostrar el resultado
        Console.WriteLine($"El tamaño promedio de archivo es de {tamanoPromedio} MB");
    }
}


﻿// 29.	Escriba un programa en C# para dividir una colección de cadenas en algunos grupos.

using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de ciudades
        string[] ciudades = { "ROMA", "LONDRES", "NAIROBI", "CALIFORNIA", "ZURICH", "NUEVA DELHI", "AMSTERDAM", "ABU DHABI", "PARIS", "NUEVA YORK" };

        // Tamaño del grupo
        int tamanoGrupo = 3;

        // Dividir la lista de ciudades en grupos usando LINQ.
        var grupos = ciudades.Select((ciudad, indice) => new { Ciudad = ciudad, IndiceGrupo = indice / tamanoGrupo })
                             .GroupBy(x => x.IndiceGrupo, y => y.Ciudad)
                             .Select(grupo => string.Join("; ", grupo) + "\n- aquí hay un grupo de ciudades -");

        // Imprimir los grupos
        Console.WriteLine("Aquí está el grupo de ciudades:\n");
        foreach (var grupo in grupos)
        {
            Console.WriteLine(grupo + "\n");
        }
    }
}


﻿// 10.	Escriba un programa en C# para aceptar los miembros de una lista a través del teclado y mostrar los miembros más que un valor específico.
using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario el número de miembros en la lista
        Console.Write("Ingrese el número de miembros en la lista: ");
        int numMiembros = int.Parse(Console.ReadLine());

        // Crear una lista para almacenar los miembros
        List<int> miembros = new List<int>();

        // Solicitar al usuario que ingrese los miembros de la lista
        for (int i = 0; i < numMiembros; i++)
        {
            Console.Write($"Miembro {i}: ");
            int miembro = int.Parse(Console.ReadLine());
            miembros.Add(miembro);
        }

        // Solicitar al usuario el valor para comparar
        Console.Write("Ingrese el valor para mostrar los miembros de la lista: ");
        int valorComparar = int.Parse(Console.ReadLine());

        // Utilizar LINQ para seleccionar los miembros mayores que el valor especificado
        var miembrosMayores = miembros.FindAll(miembro => miembro > valorComparar);

        // Mostrar los miembros mayores que el valor especificado
        Console.WriteLine("Los números mayores que " + valorComparar + " son:");
        foreach (var miembro in miembrosMayores)
        {
            Console.WriteLine(miembro);
        }
    }
}


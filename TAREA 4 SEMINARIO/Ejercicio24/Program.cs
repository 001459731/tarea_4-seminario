﻿// 24.	Escriba un programa en C# para generar un Producto Cartesiano de tres conjuntos.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir los conjuntos
        List<char> letterList = new List<char> { 'X', 'Y' };
        List<int> numberList = new List<int> { 1, 2, 3 };
        List<string> colorList = new List<string> { "verde", "naranja" };

        // Generar el Producto Cartesiano utilizando LINQ
        var cartesianProduct = from letter in letterList
                               from number in numberList
                               from color in colorList
                               select new { letra = letter, número = number, color = color };

        // Mostrar el Producto Cartesiano
        Console.WriteLine("El producto cartesiano es:");
        foreach (var item in cartesianProduct)
        {
            Console.WriteLine("{letra = " + item.letra + ", número = " + item.número + ", color = " + item.color + "}");
        }
    }
}


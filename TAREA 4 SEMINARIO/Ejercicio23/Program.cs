﻿// 23.	Escriba un programa en C# para generar un Producto Cartesiano de dos conjuntos.
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir los conjuntos
        List<char> letterList = new List<char> { 'X' };
        List<int> numberList = new List<int> { 1, 2, 3, 4 };

        // Generar el Producto Cartesiano utilizando LINQ
        var cartesianProduct = from letter in letterList
                               from number in numberList
                               select new { letterList = letter, numberList = number };

        // Mostrar el Producto Cartesiano
        Console.WriteLine("Los productos cartesianos son:");
        foreach (var item in cartesianProduct)
        {
            Console.WriteLine("{letterList = " + item.letterList + ", numberList = " + item.numberList + "}");
        }
    }
}


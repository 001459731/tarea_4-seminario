﻿//28.	Escriba un programa en C# para mostrar la lista de elementos en la matriz según la longitud de la cadena y luego por nombre en orden ascendente.
using System;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de ciudades
        string[] ciudades = { "ROMA", "PARIS", "LONDRES", "ZURICH", "NAIROBI", "ABU DHABI", "AMSTERDAM", "NUEVA DELHI" };

        // Consulta LINQ para ordenar la lista de ciudades por longitud de cadena y luego por nombre
        var resultado = ciudades.OrderBy(c => c.Length).ThenBy(c => c);

        // Imprimir el resultado ordenado
        Console.WriteLine("Aquí está la lista ordenada:");
        foreach (var ciudad in resultado)
        {
            Console.WriteLine(ciudad);
        }
    }
}

